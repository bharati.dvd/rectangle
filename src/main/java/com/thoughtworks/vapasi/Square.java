package com.thoughtworks.vapasi;

public class Square extends Rectangle{
    int dimension = 0;
    public Square(int dimension){
        super(dimension, dimension);
        this.dimension = dimension;
    }
    public int findArea(){
        Rectangle square = new Rectangle(this.dimension, this.dimension);
        int areaOfSquare = square.rectangleArea();
        return areaOfSquare;
    }
    public int findPerimeter(){
        Rectangle square = new Rectangle(this.dimension, this.dimension);
        int perimeterOfSquare = 4 * this.dimension;
        return perimeterOfSquare;
    }
}
