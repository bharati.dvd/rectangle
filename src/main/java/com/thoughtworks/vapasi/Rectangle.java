package com.thoughtworks.vapasi;

public class Rectangle {
    int length = 0;
    int breadth = 0;
    public Rectangle(int length, int breadth){
        this.length = length;
        this.breadth = breadth;
    }
    public int rectangleArea(){
        int areaOfRectangle = this.length * this.breadth;
        return areaOfRectangle;
    }
    public int rectanglePerimeter(){
        int perimeterOfRectangle = 2 * (this.length + this.breadth);
        return perimeterOfRectangle;
    }
}

