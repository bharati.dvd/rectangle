package com.thoughtworks.vapasi;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.params.provider.CsvSource;

public class SquareTest {
    @CsvSource({"2, 4", "3, 9", "0, 0"})
    void shouldReturnSquareArea(Integer dimension, Integer expected) {
        Square square = new Square(dimension);
        int actual = square.findArea();
        assertEquals(expected, actual);
    }
    @CsvSource({"2, 8", "0, 0", "5, 20"})
    void shouldReturnSquarePerimeter(Integer dimension, Integer expected) {
        Square square = new Square(dimension);
        int actual = square.findPerimeter();
        assertEquals(expected, actual);
    }
}
