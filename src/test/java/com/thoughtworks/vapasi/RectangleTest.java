package com.thoughtworks.vapasi;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.params.provider.CsvSource;

public class RectangleTest {
    @ParameterizedTest
    @CsvSource({"2, 3, 10", "3, 4, 14", "0, 0, 0"})
     void shouldReturnRectanglePerimeter(Integer length, Integer breadth, Integer expected) {
         Rectangle rectangle = new Rectangle(length, breadth);
         int actual = rectangle.rectanglePerimeter();
         assertEquals(expected, actual);
    }

    @ParameterizedTest
    @CsvSource({"2, 3, 6", "3, 4, 12", "0, 0, 0"})
    void shouldReturnRectangleArea(Integer length, Integer breadth, Integer expected) {
        Rectangle rectangle = new Rectangle(length, breadth);
        int actual = rectangle.rectangleArea();
        assertEquals(expected, actual);
    }
}